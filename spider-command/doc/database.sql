create database chajia;

create table chajia.product
(
   pid                   varchar(45) not null,
   title                  varchar(100) ,
   url                  varchar(100),
   img                varchar(200),
   state              varchar(1),
   primary key (pid)
);


create table chajia.price
(
   pid                   varchar(45) not null,
   price                 varchar(20) ,
   time                  varchar(8),
   primary key (pid)
);