package com.chajia.dao;

import com.chajia.model.Product;
import com.chajia.util.DBHelper;
import com.chajia.util.SQLHelper;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: chenchang
 * Date: 13-12-28
 * Time: 上午7:28
 * To change this template use File | Settings | File Templates.
 */
public class ProductDao {
    public boolean insert(Product product) throws Exception{
    String method="insert";
    String sql= SQLHelper.row_insert(Product.TABLE,new String[]{"pid","title","url","img","state",
            "created_at","updated_at"},null);
    int flag= DBHelper.writeO(method,sql,product.getPid(),product.getTitle(),
            product.getUrl(),product.getImg(),String.valueOf(product.getState()),
            product.getCreated_at(),product.getUpdated_at());
        return flag==1?true:false;
    }
    public static Integer getId(String pid) throws Exception{
        String method="getId";
        String sql=SQLHelper.row_select(Product.TABLE,new String[]{"id"},new String[]{"pid"},null);
        ResultSet rs =DBHelper.read(method,sql,new String[]{pid});
        Integer id=null;
        if(rs.next()){
           id=(Integer)rs.getInt(1);
        }
        return  id;
    }
    public static boolean isExist(String pid) throws Exception{
        String method="isExist";
        String sql =SQLHelper.row_select(Product.TABLE,new String[]{"pid"},new String[]{"pid"},null);
        ResultSet rs=DBHelper.read(method,sql,new String[]{pid});
        if(rs.next()){
            return true;
        }
        return false;
    }
    public  boolean updataState(char state) throws  Exception{
        String method="updataState";
        String sql=SQLHelper.row_update(Product.TABLE,new String[]{"state"},new String[]{"pid"},null) ;
        int flag = DBHelper.write(method,sql,new String[]{String.valueOf(state)});
        return flag==1?true:false;
    }
    public static boolean  isUseful(String pid) throws Exception{
        String method="isUseful";
        String sql =SQLHelper.row_select(Product.TABLE,new String[]{"state"},new String[]{"pid"},null);
        ResultSet rs=DBHelper.read(method,sql,new String[]{pid});
        if(rs.next()){
            if(rs.getString(1).compareTo("1")==0)
            return true;
        }
        return false;
    }
}
