package com.chajia.dao;

import com.chajia.model.Product;
import com.chajia.model.Shop;
import com.chajia.util.DBHelper;
import com.chajia.util.SQLHelper;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-1-19
 * Time: 上午10:23
 * To change this template use File | Settings | File Templates.
 */
public class ShopDao {
    public static boolean isExist(String sUrl) throws Exception{
        String method="isExist";
        String sql =SQLHelper.row_select(Shop.TABLE,new String[]{"id"},new String[]{"surl"},null);
        ResultSet rs=DBHelper.read(method,sql,new String[]{sUrl});
        if(rs.next()){
            return true;
        }
        return false;
    }
    public boolean insert(Shop shop) throws Exception{
        String method="insert";
        String sql= SQLHelper.row_insert(Shop.TABLE, new String[]{"surl", "valid","shopname"  ,
                "created_at", "updated_at"}, null);
        int flag= DBHelper.writeO(method, sql, shop.getsUrl(),shop.isValid(), shop.getShopName(),
                shop.getCreated_at(), shop.getUpdated_at());
        return flag==1?true:false;
    }
}
