package com.chajia.util;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 上午5:41
 * To change this template use File | Settings | File Templates.
 */
public class DateUtil {
    public final  static String format1="yyyyMMdd";
    public static String getNowDate( String format){
        GregorianCalendar ca = new GregorianCalendar();

        int month = ca.get(Calendar.MONTH) + 1;
        int date = ca.get(Calendar.DATE);
        int year = ca.get(Calendar.YEAR);
        String dd = (date > 9 ? "" : "0") + date;
        String mm = (month > 9 ? "" : "0") + month;
        String yyyy = "" + year;
        format = format.replace("yyyy", yyyy);
        format = format.replace("MM", mm);
        format = format.replace("dd", dd);
        String d = "" + date;
        String m = "" + month;
        String yy = "" + yyyy.substring(2);
        format = format.replace("yy", yy);
        format = format.replace("M", m);
        format = format.replace("d", d);
        return format;
    }
    public static Long getDate(){
        return new Date().getTime();
    }
}
