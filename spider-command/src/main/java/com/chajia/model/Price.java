package com.chajia.model;

import com.chajia.util.DateUtil;

import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-28
 * Time: 上午7:24
 * To change this template use File | Settings | File Templates.
 */
public class Price {
    public final  static String  TABLE="prices";
    private Integer product_id;
    private String price;
    private Date created_at;
    private Date updated_at;

    public Price(Integer pid, String price) {
        this.product_id = pid;
        this.price = price;
        this.created_at=new Date(DateUtil.getDate());
        this.updated_at=created_at;
    }
    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

}
