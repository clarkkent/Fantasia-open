package com.chajia.model;

import com.chajia.util.DateUtil;

import javax.print.attribute.standard.DateTimeAtCreation;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-28
 * Time: 上午7:12
 * To change this template use File | Settings | File Templates.
 */
public class Product {
    public final static String TABLE="products";
    private String pid;
    private String title;
    private String url;
    private String img;
    private char state='1';//1:true url有效  0:false url无效
    private Date created_at;
    private Date updated_at;

    public Product(String pid, String title, String url, String img) {
        this.pid = pid;
        this.title = title;
        this.url = url;
        this.img = img;
        this.created_at=new Date(DateUtil.getDate());
        this.updated_at=created_at;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public char getState() {
        return state;
    }

    public void setState(char state) {
        this.state = state;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }
}
